import argparse
import sys

import ROOT
from configuration import Configuration
from helper_functions import makedir, saver
from plotting_tools import readdir
from ROOT import gStyle
from tqdm import tqdm

sys.dont_write_bytecode = True


# Get bin type and name of file for 2D maps


def parse_args():
    """
    Argument parser
    """
    parser = argparse.ArgumentParser(description="Plot efficiency maps")

    parser.add_argument(
        "-c", "--config", required=True, type=str, help="Path to config file."
    )
    parser.add_argument(
        "-t",
        "--tag",
        required=False,
        default="",
        help="Filename tag",
    )
    parser.add_argument(
        "-b",
        "--bin_type",
        required=False,
        default="c",
        help="Binning type (c - calibrated, f - finely).",
    )

    args = parser.parse_args()
    return args


def plot_2d_maps(config, tag, bin_type):

    if bin_type == "f":
        dir_type = "finely_binned"
        binning = "finely"
    elif bin_type == "c":
        dir_type = "calibration_binned"
        binning = "calibrated"
    else:
        dir_type = "calibration_binned"
        binning = "calibrated"
    k = 0
    maps = {}
    rent = []

    ROOT.gROOT.SetBatch(1)

    print("-----------------------------")
    # Create respective directories for binned maps
    makedir("vr_maps_2D")
    makedir("vr_maps_2D", dir_type)
    dir = "vr_maps_2D/" + dir_type

    ROOT.gErrorIgnoreLevel = ROOT.kWarning
    # c=0

    for jetcol, tagger, wp, flav, sample in tqdm(
        config.get_hist_names(sample_names=True)
    ):
        if wp == "Continuous":
            filec = dir_type + "/" + tag + "eta_vs_pt_cont.root"
            fc = ROOT.TFile.Open(filec)
            hc, _ = readdir(maps, fc, k, rent)  # Create dictionary of all the maps
            for twbin in config.binning["twbins"][tagger]:

                # continue
                c = ROOT.TCanvas("c", "c", 1000, 750)
                pathname = "/".join(
                    [binning, tagger, jetcol, wp, flav, twbin, sample]
                )  # Dictionary key name
                plotname = "_".join(
                    [
                        binning,
                        config.rename_taggers.get(tagger, tagger),
                        jetcol,
                        wp,
                        flav,
                        twbin,
                        sample,
                    ]
                )
                # print pathname
                hc[pathname].Draw("col Z")
                saver(
                    c,
                    plotname,
                    dir,
                    config.rename_taggers.get(tagger, tagger),
                    jetcol,
                    wp,
                    twbin,
                    flav,
                )
                del c
        else:
            file = dir_type + "/" + tag + "eta_vs_pt.root"
            f = ROOT.TFile.Open(file)

            h, _ = readdir(maps, f, k, rent)  # Create dictionary of all the maps

            c = ROOT.TCanvas("c", "c", 1000, 750)
            pathname = "/".join(
                [binning, tagger, jetcol, wp, flav, sample]
            )  # Dictionary key name
            plotname = "_".join(
                [
                    binning,
                    config.rename_taggers.get(tagger, tagger),
                    jetcol,
                    wp,
                    flav,
                    sample,
                ]
            )  # Name of plot
            h[pathname].Draw("col Z")  # Draw the histogram in colour
            saver(
                c,
                plotname,
                dir,
                config.rename_taggers.get(tagger, tagger),
                jetcol,
                wp,
                flav,
                sample,
            )  # Save the 2D map in pdf format
            del c
        del pathname
        del plotname


def main():
    args = parse_args()

    config = Configuration(args.config)

    plot_2d_maps(config, args.tag, args.bin_type)


if __name__ == "__main__":
    main()
