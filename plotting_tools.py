import math

from helper_functions import getHistContent, save_plot2, saver2
from plot_eff import VarVsEff, VarVsEffPlot
from puma.utils.histogram import hist_ratio
import re

texlist = []

y_labels = {
    "B": "$b$-jet efficiency",
    "C": "$c$-jet mistag rate",
    "L": "Light-jet mistag rate",
    "T": "$\\tau$-jet mistag rate",
}
flav_remap = {
    "B": "$b$",
    "C": "$c$",
    "L": "Light",
    "T": "$\\tau$",
}


def find_max(h_list, logscale=False):
    old_max = None
    for h in h_list:
        max = h.GetBinContent(h.GetMaximumBin())  # Find max value in Y for sample
        # Set new max and min values for scaling of Y axis
        if old_max is None:
            old_max = max
        elif old_max < max:
            old_max = max
    if logscale and max > 0:
        return 10 ** math.ceil(math.log10(max))
    else:
        return max + 0.3


def find_min(h_list, logscale=False):
    old_min = None
    for h in h_list:
        min = h.GetBinContent(h.GetMinimumBin())  # Find max value in Y for sample
        # Set new max and min values for scaling of Y axis
        if old_min is None:
            old_min = min
        elif old_min > min:
            old_min = min
    if logscale and min > 0:
        return 10 ** math.floor(math.log10(min))
    else:
        return min - 0.2


def readdir(MyArr, dir, lvl=0, parents=None, name_list=[]):

    if parents == None:
        parents = []
    if lvl > 0:
        parents.append(dir.GetName())
    keys = dir.GetListOfKeys()

    for key in keys:
        # print key.GetName()
        # print '\t',parents[:]
        if key.IsFolder():
            # print key.ReadObj()
            readdir(MyArr, key.ReadObj(), lvl + 1, parents[:])
        else:
            name = "/".join(parents)
            # print name
            MyArr[name] = dir.Get(key.GetName())
            name_list.append(name)
    return MyArr, name_list


def create_map(h, path_name, sample_list, info, type, config):
    tagger = info[0]
    jetcol = info[1]
    wp = info[2]
    flav = info[3]
    bin = info[4]
    if "eff_vs_pt" in type:
        xlabel = "$p_{T}$ [GeV]"
    else:
        xlabel = "$\eta$"
    if wp == "Continuous":
        twbin = info[5].replace(" ", "-")
        wp_digit = twbin
    else:
        wp_digit = wp.split("_")[-1]
    if wp == "Continuous":
        hname = f"{tagger}_{wp}_{flav}_{bin}_{twbin}"
    else:
        hname = f"{tagger}_{wp}_{flav}_{bin}"  # Set title name
    h_list = {}
    for sample in sample_list:
        path = path_name + "/" + sample
        h_list[sample] = h[path].Clone()
    # h_list = [h[p] for p in path_list]
    max = find_max(h_list.values(), logscale=(flav != "B"))
    min = find_min(h_list.values(), logscale=(flav != "B"))
    plot_obj = VarVsEffPlot(
        ylabel=y_labels[flav],
        xlabel=xlabel,
        logy=(flav != "B"),
        atlas_second_tag=f"$\\sqrt{{s}}=13$ TeV, $|\eta| < 2.5$, {flav_remap[flav]}-jets \n{config.rename_taggers.get(tagger, tagger)} $\\varepsilon_b={wp_digit}\%$ Single cut WP \nNominal: Powheg + Pythia8",
        n_ratio_panels=1,
        figsize=(7.0, 5.25),
        leg_fontsize=8,
        y_scale=1.6 if flav == "B" else 2,
        # ymin=min,
        # ymax=max,
        dpi=300,
    )
    plot_obj_ratio = VarVsEffPlot(
        ylabel="Ratio " + y_labels[flav],
        xlabel=xlabel,
        logy=False,
        atlas_second_tag=f"$\\sqrt{{s}}=13$ TeV, $|\eta| < 2.5$, {flav_remap[flav]}-jets \n{config.rename_taggers.get(tagger, tagger)} $\\varepsilon_b={wp_digit}\%$ Single cut WP \nNominal: Powheg + Pythia8",
        n_ratio_panels=0,
        figsize=(7.0, 5.25),
        leg_fontsize=8,
        y_scale=1.6,
        dpi=300,
        # atlas_fontsize=12,
    )
    # Set up
    denom_vals, denom_errors, _ = getHistContent(h_list["PhPy8EG"])
    for key, value in h_list.items():
        vals, errors, bins = getHistContent(value)
        plot_obj.add(
            VarVsEff(
                eff=vals,
                error=errors,
                bins=bins,
                label=config.samples[key]["plot_name"],
                marker=config.samples[key]["marker"],
                colour=config.samples[key]["plot_color"],
            ),
            reference=(key == "PhPy8EG"),
        )
        if key != "PhPy8EG":
            ratio, ratio_errors = hist_ratio(
                vals, denom_vals, errors, denom_errors, step=False
            )
            plot_obj_ratio.add(
                VarVsEff(
                    eff=ratio,
                    error=ratio_errors,
                    bins=bins,
                    label=config.samples[key]["plot_name"],
                    marker=config.samples[key]["marker"],
                    colour=config.samples[key]["plot_color"],
                )
            )
    plot_obj.draw()
    plot_obj_ratio.draw()
    if wp == "Continuous":
        saver2(
            plot_obj,
            hname,
            type,
            config.rename_taggers.get(tagger, tagger),
            jetcol,
            str(wp),
            flav,
            bin,
            twbin,
        )
        saver2(
            plot_obj_ratio,
            hname + "_ratio",
            type,
            config.rename_taggers.get(tagger, tagger),
            jetcol,
            str(wp),
            flav,
            bin,
            twbin,
        )
    else:
        save_plot2(
            plot_obj,
            type,
            config.rename_taggers.get(tagger, tagger),
            jetcol,
            wp,
            flav,
            bin,
            hname,
        )
        save_plot2(
            plot_obj_ratio,
            type,
            config.rename_taggers.get(tagger, tagger),
            jetcol,
            wp,
            flav,
            bin,
            hname + "_ratio",
        )


def create_map_wp_binned(
    h, sample_list, bin_type, tagger, jetcol, wp, flav, binx, twbins, wp_local, config
):

    xlabel = "Working point bin"
    wps = [re.findall("FixedCutBEff_([0-9]{2})",wp)[0] for wp in config.working_points if "Continuous" not in wp]
    wps = wps[::-1]
    wps.append("0")
    wps.insert(0,"100")
    xlabels_wps = [f"[{wps[i]},{wps[i+1]}]%" for i in range(len(wps) - 1)]

    values = {sample: {} for sample in sample_list}
    errors = {sample: {} for sample in sample_list}
    pt_bins = None
    # ymins = ['B' : 1e-2, 'C'
    for sample in sample_list:
        for twbin in twbins:
            path_name = "/".join([bin_type, tagger, jetcol, wp, flav, binx, twbin])
            path = path_name + "/" + sample
            vals, errs, pt_bins = getHistContent(h[path].Clone())
            values[sample][twbin] = vals
            errors[sample][twbin] = errs
    for pt_bin_idx in range(len(pt_bins) - 1):
        plot_obj = VarVsEffPlot(
            ylabel="Ratio to Nominal fraction of jets",
            xlabel=xlabel,
            # logy=True,
            atlas_second_tag=f"$\\sqrt{{s}}=13$ TeV, $|\eta| < 2.5$, {flav_remap[flav]}-jets \n{config.rename_taggers.get(tagger, tagger)} $[{pt_bins[pt_bin_idx]},{pt_bins[pt_bin_idx+1]}]$ GeV Jet $p_T$ Bin \nNominal: Powheg + Pythia8",
            n_ratio_panels=1,
            figsize=(7.0, 5.25),
            leg_fontsize=8,
            y_scale=2,
            dpi=300,
        )
        plot_obj_ratio = VarVsEffPlot(
            ylabel="Ratio fraction of jets",
            xlabel=xlabel,
            logy=False,
            atlas_second_tag=f"$\\sqrt{{s}}=13$ TeV, $|\eta| < 2.5$, {flav_remap[flav]}-jets \n{config.rename_taggers.get(tagger, tagger)} $[{pt_bins[pt_bin_idx]},{pt_bins[pt_bin_idx+1]}]$ GeV Jet $p_T$ Bin \nNominal: Powheg + Pythia8",
            n_ratio_panels=0,
            figsize=(7.0, 5.25),
            leg_fontsize=8,
            y_scale=1.6,
            dpi=300,
            # atlas_fontsize=12,
        )
        denom_vals = [values["PhPy8EG"][twbin][pt_bin_idx] for twbin in twbins]
        denom_errors = [errors["PhPy8EG"][twbin][pt_bin_idx] for twbin in twbins]
        for sample in sample_list:
            plot_vals = [values[sample][twbin][pt_bin_idx] for twbin in twbins]
            plot_errors = [errors[sample][twbin][pt_bin_idx] for twbin in twbins]
            plot_bins = list(range(len(twbins) + 1))
            plot_obj.add(
                VarVsEff(
                    eff=plot_vals,
                    error=plot_errors,
                    bins=plot_bins,
                    label=config.samples[sample]["plot_name"],
                    marker=config.samples[sample]["marker"],
                    colour=config.samples[sample]["plot_color"],
                ),
                reference=(sample == "PhPy8EG"),
            )
            if sample != "PhPy8EG":
                ratio, ratio_errors = hist_ratio(
                    plot_vals, denom_vals, plot_errors, denom_errors, step=False
                )
                plot_obj_ratio.add(
                    VarVsEff(
                        eff=ratio,
                        error=ratio_errors,
                        bins=plot_bins,
                        label=config.samples[sample]["plot_name"],
                        marker=config.samples[sample]["marker"],
                        colour=config.samples[sample]["plot_color"],
                    )
                )
        plot_obj.set_ratio_label(1, "Ratio")
        plot_obj.draw(
            #xlables=["[100,85]%", "[85,77]%", "[77,70]%", "[70,60]%", "[60,0]%"]
            xlables = xlabels_wps
        )
        plot_obj_ratio.draw(
            #xlables=["[100,85]%", "[85,77]%", "[77,70]%", "[70,60]%", "[60,0]%"]
            xlables = xlabels_wps
        )
        hname = f"{tagger}_{wp}_{flav}_{binx}_[{pt_bins[pt_bin_idx]:g}-{pt_bins[pt_bin_idx+1]:g}]"
        saver2(
            plot_obj,
            hname,
            wp_local,
            config.rename_taggers.get(tagger, tagger),
            jetcol,
            str(wp),
            flav,
            binx,
        )
        saver2(
            plot_obj_ratio,
            hname + "_ratio",
            wp_local,
            config.rename_taggers.get(tagger, tagger),
            jetcol,
            str(wp),
            flav,
            binx,
        )
