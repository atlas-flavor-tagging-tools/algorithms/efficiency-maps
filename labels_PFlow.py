import itertools
import os
import sys


def labels():
    # Why is this here again ? What is the point of the config files ??
    categories = ["maps_a", "maps_d", "maps_e"]
    jetCols = ["AntiKt4EMPFlowJets"]
    taggers = ["DL1dv00", "DL1dv01", "GN120220509","GN2v01"]
    #workingPoints = ["FixedCutBEff_" + x for x in ["60", "70", "77", "85"]] + [
    #    "Continuous"
    #]
    workingPoints = ["FixedCutBEff_" + x for x in ["65", "70", "77", "85","90"]] + [
        "Continuous"
    ]
    flavours = ["B", "C", "L", "T"]
    sample_names = [
        "PhPy8EG",
        "Sherpa2210",
        "PowhegHerwig7",
        "aMcAtNloHerwig7EvtGen",
        "aMcAtNloPy8EvtGen",
    ]  # , "aMcAtNloPy8", "aMcAtNloHerwig7", ]
    twbins = {
        "GN120220509": [
            "[-100-1.253]",
            "[1.253-2.602]",
            "[2.602-3.642]",
            "[3.642-5.135]",
            "[5.135-100]",
        ],
        "DL1dv00": [
            "[-100-0.93]",
            "[0.93-2.443]",
            "[2.443-3.494]",
            "[3.494-4.884]",
            "[4.884-100]",
        ],
        "DL1dv01": [
            "[-100-0.948]",
            "[0.948-2.456]",
            "[2.456-3.493]",
            "[3.493-4.854]",
            "[4.854-100]",
        ],
        "GN2v01": [
        "[-100:-1.351]",
        "[-1.351:-0.396]",
        "[-0.396:0.828]",
        "[0.828:1.877]",
        "[1.877:2.658]",
        "[2.658:100]",
      ],
    }
    return jetCols, taggers, workingPoints, flavours, sample_names, twbins


def it(flav, bins="calibrated"):
    if bins == "calibrated":
        if flav == "L":
            # ptbinning,etabinning = [[10.0,30.0,60.0,150.0,300.0],[0,2.5]]
            ptbinning, etabinning = [[20.0, 50.0, 100.0, 150.0, 300.0], [0.0, 2.5]]
        elif flav == "B":
            ptbinning, etabinning = [
                [20.0, 30.0, 40.0, 60.0, 85.0, 110.0, 140.0, 175.0, 250.0, 400.0],
                [0, 2.5],
            ]
            # ptbinning,etabinning = [[10.0,20.0,30.0,60.0,100.0,250.0],[0,2.5]]
        elif flav == "C":
            ptbinning, etabinning = [[20.0, 40.0, 65.0, 140.0, 250.0], [0, 2.5]]
            # ptbinning,etabinning = [[10.0,20.0,40.0,65.0,140.0],[0,2.5]]
        else:
            ptbinning, etabinning = [[20.0, 40.0, 65.0, 140.0, 250.0], [0, 2.5]]
            # ptbinning,etabinning = [[10.0,20.0,40.0,65.0,140.0],[0,2.5]]
    elif bins == "finely":
        if flav == "L":
            ptbinning, etabinning = [20.0, 50.0, 100.0, 150.0, 300.0], [
                0,
                0.6,
                1.2,
                1.8,
                2.5,
            ]
            # ptbinning,etabinning = [10,20,30,40,60,80,110,140,160,200,400],[0.0,1,2.5]
        elif flav == "C":
            ptbinning, etabinning = [20.0, 40.0, 65.0, 140.0, 250.0], [
                0,
                0.6,
                1.2,
                1.8,
                2.5,
            ]
        elif flav == "T":
            ptbinning, etabinning = [20.0, 40.0, 65.0, 140.0, 250.0], [
                0,
                0.6,
                1.2,
                1.8,
                2.5,
            ]
            # ptbinning,etabinning = [10,20,30,40,60,80,110,140,160,240,400],[0.0,1,2.5]
        elif flav == "B":
            ptbinning, etabinning = [
                20.0,
                30.0,
                40.0,
                60.0,
                85.0,
                110.0,
                140.0,
                175.0,
                250.0,
                400.0,
            ], [0, 0.6, 1.2, 1.8, 2.5]
            # ptbinning,etabinning = [10,20,30,40,60,80,110,140,160,200,260,320,400],[0.0,1,1.5,2.5]
    return ptbinning, etabinning


def data_extract():
    datasets = {
        #        "aMcAtNloHerwig7": "data/Herwig7_out.root",
        #        "aMcAtNloPy8": "data/Py8_out.root",
        "Sherpa2210": "data/Sh_maxHTavrgTopPT_SSC.root",
        "PhPy8EG": "data/PhPy8EG_nonallhad.root",
        "PowhegHerwig7": "data/PowhegHerwig7EvtGen.root",
        "aMcAtNloHerwig7EvtGen": "data/aMcAtNloHerwig7EvtGen_ttbar.root",
        "aMcAtNloPy8EvtGen": "data/aMcAtNloPy8EvtGen.root",
        "PhH7EG_H7UE": "data/PhH7EG_H7UE_tt_hdamp258p75_721.root",
    }
    return datasets


sample_ids = {
    #   "aMcAtNloHerwig7": ["4002", "aMcAtNloHerwig7"],
    #    "aMcAtNloPy8": ["4001", "aMcAtNloPy8"],
    "Sherpa2210": ["700122", "Sherpa2210"],
    "PhPy8EG": ["410470", "Pythia8EvtGen"],
    "PhPy8EGhdamp517p5" : ["410480","PhPy8EGhdamp517p5"],
    "PhH713EG": ["411233", "PowhegHerwig7"],
    "aMcAtNloHerwig7": ["412116", "aMcAtNloHerwig7"],
    "aMcAtNloPy8": ["410464", "aMcAtNloPy8"],
    "PhH7EG_H7UE": ["600666", "PhH7EG_H7UE"],
    "Sherpa221":["410250","Sherpa221"],
    "Sherpa2212":["700660","Sherpa2212"]
}

map_folder = "JetFtagEffPlots"
