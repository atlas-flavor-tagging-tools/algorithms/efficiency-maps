import argparse
import json
import os
import sys

import ROOT
from binning_functions import findPtEtaBinning
from configuration import Configuration
from get_hist import get_hists
from helper_functions import (applyWeights, create_3d_hist, getWeights,
                              rebin2D, write_outfile)
from ROOT import gStyle

sys.dont_write_bytecode = True



def parse_args():
    """
    Argument parser
    """
    parser = argparse.ArgumentParser(description="Create eff maps configs.")

    parser.add_argument(
        "-c", "--config", required=True, type=str, help="Path to config file."
    )
    parser.add_argument("-m", "--maps", required=True, type=str, help="Maps filename")
    parser.add_argument(
        "-t",
        "--tag",
        required=False,
        default="",
        help="Output tag",
    )
    parser.add_argument(
        "-b",
        "--bin_type",
        required=False,
        default="c",
        help="Binning type (c - calibrated, f - finely).",
    )

    args = parser.parse_args()
    return args


def create_hists(config, maps_file, output_tag, bin_type):
    ROOT.gROOT.SetBatch(1)
    uniform_finely_binned = True  # enforce the same pt/eta binning on all sub-samples

    # Choose between finely and calibrated maps
    if bin_type == "f":
        directory = "finely_binned"
        bins = "finely"
    elif bin_type == "c":
        directory = "calibration_binned"
        bins = "calibrated"
    else:
        directory = "calibration_binned"
        bins = "calibrated"

    # Choose output root file names
    outputfilename = directory + "/" + output_tag + "eta_vs_pt.root"
    outputfilenamex = directory + "/" + output_tag + "eff_vs_pt.root"
    outputfilenamey = directory + "/" + output_tag + "eff_vs_eta.root"
    outputfilenamec = directory + "/" + output_tag + "eta_vs_pt_cont.root"
    outputfilenamet = directory + "/" + output_tag + "eff_vs_tw.root"
    outputfilename_total = directory + "/" + output_tag + "total.root"
    outputfilename_pass = directory + "/" + output_tag + "pass.root"
    # Create directory if it exists
    if not os.path.exists(directory):
        os.mkdir(directory)
    # Remake root files if they already exist
    if os.path.exists(outputfilename):
        os.system("rm " + outputfilename)
    if os.path.exists(outputfilenamex):
        os.system("rm " + outputfilenamex)
    if os.path.exists(outputfilenamey):
        os.system("rm " + outputfilenamey)
    if os.path.exists(outputfilenamec):
        os.system("rm " + outputfilenamec)
    if os.path.exists(outputfilenamet):
        os.system("rm " + outputfilenamet)
    if os.path.exists(outputfilename_total):
        os.system("rm " + outputfilename_total)
    if os.path.exists(outputfilename_pass):
        os.system("rm " + outputfilename_pass)

    map_configs = json.load(open(maps_file, "r"))  # Read the map names out

    listofmaps = [
        str(x) for x in map_configs.keys()
    ]  # Create a list of the map names which are the keys

    print(len(listofmaps))

    temp_outputfile = ROOT.TFile("temp.root", "recreate")
    # Enumerate the map list
    for map_i in range(len(listofmaps)):

        map_name = listofmaps[map_i]  # Get key for the map
        map_name = str(map_name)

        print(str(map_i) + "   " + map_name)

        map_config = map_configs[map_name]  # Get all the items for each map as one term
        
        # Get all the map properties isolated
        tagger = str(map_config["tagger"])
        wp = str(map_config["wp"])
        flav = str(map_config["flav"])
        jetcol = str(map_config["jetcol"])
        samplelist = [str(x) for x in map_config["sample_names"]]
        nominal_sample = samplelist[1]  # Choose PhPy8EG as the nominal sample
        ptbinning = config.binning[bins][flav][
            "ptbinning"
        ]  # Choose the binnings for pt and eta
        etabinning = config.binning[bins][flav]["etabinning"]

        if wp == "Continuous":
            tagweight_bins = map_config["tagweight_bins"]
        # if flav != 'L':
        # 	continue
        # if wp == 'FixedCutBEff_70' or wp == 'FixedCutBEff_77' or wp == 'FixedCutBEff_85':
        # 	continue
        l_pt = len(ptbinning)
        l_eta = len(etabinning)
        t_h, p_h = get_hists(
            temp_outputfile, map_config
        )  # Get total and passed histograms from output files and temporarily store them in an outfile
        
        # Validation check - if bins are empty
        total_histogram = []
        pass_histograms = []
        # Loop over sample names
        for sample_i, sample_name in enumerate(samplelist):
            h_total_nom = t_h[nominal_sample]
            h_total = ""
            # we can get the number of bins (instead of hardcoding 5 everywhere...)
            nb_bins = len(p_h[sample_name]) 
            if wp == "Continuous":
                h_total = t_h[sample_name][0]
                pass_histograms = []
                for bin_i in range(nb_bins):
                    # total_histogram.append( t_h[sample_name][0].Clone('total_'+str(bin_i)) )
                    pass_histograms.append(p_h[sample_name][bin_i])
                    # print str(len(total_histogram))+'  '+str(len(pass_histograms))+'  '+str(len(samplelist))
                # print 'Length of passed hist'+str(len(pass_histograms))
                h_total_nom = h_total_nom[0]
                h_ref = rebin2D("_ref", h_total_nom, ptbinning, etabinning)
                h_t = rebin2D("_total", h_total, ptbinning, etabinning)
                h_p = [
                    rebin2D(
                        "_pass" + str(sample_i) + "_" + str(bin_i),
                        pass_histograms[bin_i],
                        ptbinning,
                        etabinning,
                    )
                    for bin_i in range(nb_bins)
                ]
                weights = getWeights(h_ref, h_t)
                h_total = applyWeights(weights, h_t)
                h_pass = [applyWeights(weights, h_p_i) for h_p_i in h_p]
                h_t = rebin2D("_total", h_total, ptbinning, etabinning)
                h_p = [
                    rebin2D("_pass_" + str(bin_i), h_pass[bin_i], ptbinning, etabinning)
                    for bin_i in range(nb_bins)
                ]

                effs = [h_t.Clone("eff_" + str(bin_i)) for bin_i in range(nb_bins)]
                Title = ("%s_%s_%s_%s_%s") % (tagger, wp, flav, sample_name, bins)

                for bin_i in range(nb_bins):
                    effs[bin_i].Divide(h_p[bin_i], h_t, 1.0, 1.0, "B")
                eff_cont = create_3d_hist(
                    effs, tagweight_bins, ptbinning, etabinning
                )  # Create 3D continuous maps
                # Map formatting
                # print 'Continuous sample: '+sample_name
                eff_cont.SetTitle(Title)
                eff_cont.GetXaxis().SetTitle("pt")
                eff_cont.GetYaxis().SetTitle("abseta")
                eff_cont.GetZaxis().SetTitle("tagweight")
                eff_cont.SetStats(0)
                write_outfile(
                    outputfilename,
                    eff_cont,
                    "hist_" + str(Title) + "_Eff",
                    bins,
                    tagger,
                    jetcol,
                    wp,
                    flav,
                    sample_name,
                )  # Write out rootfile of 3D efficiency maps
                for bin_i in range(len(h_p)):
                    # why is tb hardcoded when it is given in the config file?
                    tb = (
                        "["
                        + str(tagweight_bins[bin_i])
                        + ":"
                        + str(tagweight_bins[bin_i + 1])
                        + "]"
                    )  # name the tagweight bin range
                    efft = eff_cont.Clone("efft")
                    efft.GetZaxis().SetRange(bin_i + 1, bin_i + 1)
                    efft2d = efft.Project3D("yx")
                    Titlet = (
                        "VR Eta vs Pt"
                        + "_"
                        + tagger
                        + "_"
                        + wp
                        + "_"
                        + flav
                        + "_"
                        + sample_name
                        + "_"
                        + tb
                        + "_"
                        + bins
                    )
                    efft2d.SetTitle(Titlet)
                    efft2d.GetXaxis().SetTitle("pt")
                    efft2d.GetYaxis().SetTitle("abseta")
                    write_outfile(
                        outputfilenamec,
                        efft2d,
                        "hist_" + str(Title) + "_Eff",
                        bins,
                        tagger,
                        jetcol,
                        wp,
                        flav,
                        tb,
                        sample_name,
                    )
                    del efft
                    del efft2d
                    for i in range(l_eta - 1):
                        feffx = eff_cont.ProjectionX(
                            "px", i + 1, i + 1, bin_i + 1, bin_i + 1
                        )  # Choose 2D efficiency maps of pt range for specific eta bins
                        binx = (
                            "eta={"
                            + str(etabinning[i])
                            + "-"
                            + str(etabinning[i + 1])
                            + "}"
                        )  # Create bin name
                        Titlex = (
                            "VR Efficiency vs Pt"
                            + "_"
                            + tagger
                            + "_"
                            + wp
                            + "_"
                            + flav
                            + "_"
                            + sample_name
                            + binx
                            + "_"
                            + tb
                        )
                        # print Titlex
                        feffx.SetTitle(Titlex)
                        feffx.GetYaxis().SetRangeUser(0.0, 1.0)
                        feffx.GetXaxis().SetTitle("pt")
                        feffx.GetYaxis().SetTitle("Efficiency")
                        feffx.SetStats(0)
                        write_outfile(
                            outputfilenamex,
                            feffx,
                            "hist_" + str(sample_name) + "_Eff",
                            bins,
                            tagger,
                            jetcol,
                            wp,
                            flav,
                            binx,
                            tb,
                            sample_name,
                        )  # Write out 2D efficiency map
                        del feffx
                    for j in range(l_pt - 1):
                        feffy = eff_cont.ProjectionY(
                            "py", j + 1, j + 1, bin_i + 1, bin_i + 1
                        )  # Choose 2D efficiency maps of eta range for specific pt bins
                        biny = (
                            "pt={"
                            + str(ptbinning[j])
                            + "-"
                            + str(ptbinning[j + 1])
                            + "}"
                        )  # Create bin name
                        Titley = (
                            "VR Efficiency vs Eta"
                            + "_"
                            + tagger
                            + "_"
                            + wp
                            + "_"
                            + flav
                            + "_"
                            + sample_name
                            + biny
                            + "_"
                            + tb
                        )
                        # print Titley
                        feffy.SetTitle(Titley)
                        feffy.GetYaxis().SetRangeUser(0.0, 1.0)
                        feffy.GetXaxis().SetTitle("eta")
                        feffy.GetYaxis().SetTitle("Efficiency")
                        feffy.SetStats(0)
                        write_outfile(
                            outputfilenamey,
                            feffy,
                            "hist_" + str(sample_name) + "_Eff",
                            bins,
                            tagger,
                            jetcol,
                            wp,
                            flav,
                            biny,
                            tb,
                            sample_name,
                        )  # Write out 2D efficiency map
                        del feffy
                eff_t = eff_cont.Project3D("xz")  # Choose tagweight and pt axis
                for j in range(l_pt - 1):
                    feffx = eff_t.ProjectionX(
                        "px", j + 1, j + 1
                    )  # Select efficiency vs tagweight
                    binx = (
                        "pt={" + str(ptbinning[j]) + "-" + str(ptbinning[j + 1]) + "}"
                    )  # Pt bin
                    Titlex = (
                        "VR Efficiency vs TW"
                        + "_"
                        + tagger
                        + "_"
                        + wp
                        + "_"
                        + flav
                        + "_"
                        + sample_name
                        + "_"
                        + binx
                    )
                    # print Titley
                    feffx.SetTitle(Titlex)
                    feffx.GetYaxis().SetRangeUser(0.0, 1.0)
                    feffx.GetXaxis().SetTitle("Operating Point Bin")
                    feffx.GetYaxis().SetTitle("Efficiency")
                    feffx.SetStats(0)
                    write_outfile(
                        outputfilenamet,
                        feffx,
                        "hist_cont" + str(sample_name) + "_Eff",
                        bins,
                        tagger,
                        jetcol,
                        wp,
                        flav,
                        binx,
                        sample_name,
                    )  # Write out 1D efficiency map
                    del feffx
                for h in effs:
                    del h
                del h_t
                del eff_cont
                del h_p
                del h_total
                del weights
                del h_pass
                del h_ref

            else:
                h_total = t_h[sample_name]  # Total hits histogram
                h_pass = p_h[sample_name]  # Passed hits histogram
                ht_clone = h_total.Clone("ht_clone")
                Title0 = (
                    "VR Total_" + tagger + "_" + wp + "_" + flav + "_" + sample_name
                )
                # ht_clone.SetTitle(Title0)
                bin0 = ht_clone.ProjectionX("px", 0, -1)
                entries = bin0.GetEntries()
                print("%s entries: %s" % (sample_name, str(entries)))
                # print Title0
                # bin0 = h_total
                bin0.SetTitle(Title0)
                # bin0.GetXaxis().SetTitle('pt')
                # bin0.GetYaxis().SetTitle('Total')
                # Validate
                write_outfile(
                    outputfilename_total,
                    bin0,
                    "hist_" + str(Title0) + "_Eff",
                    bins,
                    tagger,
                    jetcol,
                    wp,
                    flav,
                    sample_name,
                )
                hp_clone = h_pass.Clone("hp_clone")
                bin1 = hp_clone.ProjectionX("px", 0, -1)
                Title1 = (
                    "VR Passed_" + tagger + "_" + wp + "_" + flav + "_" + sample_name
                )
                # bin1 = h_pass
                bin1.SetTitle(Title1)
                # bin1.GetXaxis().SetTitle('pt')
                # bin1.GetYaxis().SetTitle('Passed')
                # validation(bin1,Title1)
                write_outfile(
                    outputfilename_pass,
                    bin1,
                    "hist_" + str(Title1) + "_Eff",
                    bins,
                    tagger,
                    jetcol,
                    wp,
                    flav,
                    sample_name,
                )
                del bin0
                del bin1
                # print 'Made it here ' + str(map_i)
                h_ref = rebin2D(
                    "_ref", h_total_nom, ptbinning, etabinning
                )  # Rebin reference histogram
                h_t = rebin2D(
                    "_total", h_total, ptbinning, etabinning
                )  # Rebin total histogram
                h_p = rebin2D(
                    "_pass", h_pass, ptbinning, etabinning
                )  # Rebin pass histogram

                if not uniform_finely_binned:
                    ptbinning, etabinning = findPtEtaBinning(
                        flav, jetcol, [h_p], [h_t], eta_range=[0, 2.5], eta_step=0.2
                    )

                    print(sample_name)
                    print(ptbinning)
                    print(etabinning)
                    print("-----------")
                    h_t = rebin2D("_total", h_t, ptbinning, etabinning)
                    h_p = rebin2D("_pass", h_p, ptbinning, etabinning)

                weights = getWeights(h_ref, h_t)  # Obtain the weights
                h_total = applyWeights(
                    weights, h_t
                )  # Apply the weights on the total histogram

                h_pass = applyWeights(
                    weights, h_p
                )  # Apply the weights on the pass histogram
                h_t = rebin2D(
                    "_total", h_total, ptbinning, etabinning
                )  # Rebin total histogram
                h_p = rebin2D(
                    "_pass", h_pass, ptbinning, etabinning
                )  # Rebin pass histogram

                eff = h_t.Clone("eff")  # Clone the total histogram
                eff.Divide(
                    h_p, h_t, 1.0, 1.0, "B"
                )  # Get the ratio between the pass and total histograms to get efficiency map
                # Eta vs pt 2D efficiency maps
                Title = ("%s_%s_%s_%s_%s") % (tagger, wp, flav, sample_name, bins)
                eff.SetTitle(Title)
                eff.GetXaxis().SetTitle("pt")
                eff.GetYaxis().SetTitle("abseta")
                # eff.GetZaxis().SetTitle('tagweight')
                eff.SetStats(0)
                write_outfile(
                    outputfilename,
                    eff,
                    "hist_" + str(Title) + "_Eff",
                    bins,
                    tagger,
                    jetcol,
                    wp,
                    flav,
                    sample_name,
                )

                l_pt = len(ptbinning)
                l_eta = len(etabinning)
                for i in range(l_eta - 1):
                    feffx = eff.ProjectionX("px", i + 1, i + 1)
                    binx = (
                        "eta={"
                        + str(etabinning[i])
                        + "-"
                        + str(etabinning[i + 1])
                        + "}"
                    )
                    Titlex = (
                        "VR Efficiency vs Pt"
                        + "_"
                        + tagger
                        + "_"
                        + wp
                        + "_"
                        + flav
                        + "_"
                        + sample_name
                        + binx
                    )
                    # print Titlex
                    feffx.SetTitle(Titlex)
                    feffx.GetYaxis().SetRangeUser(0.0, 1.0)
                    feffx.GetXaxis().SetTitle("pt")
                    feffx.GetYaxis().SetTitle("Efficiency")
                    feffx.SetStats(0)
                    write_outfile(
                        outputfilenamex,
                        feffx,
                        "hist_" + str(sample_name) + "_Eff",
                        bins,
                        tagger,
                        jetcol,
                        wp,
                        flav,
                        binx,
                        sample_name,
                    )
                    del feffx
                for j in range(l_pt - 1):
                    feffy = eff.ProjectionY("py", j + 1, j + 1)
                    biny = (
                        "pt={" + str(ptbinning[j]) + "-" + str(ptbinning[j + 1]) + "}"
                    )
                    Titley = (
                        "VR Efficiency vs Eta"
                        + "_"
                        + tagger
                        + "_"
                        + wp
                        + "_"
                        + flav
                        + "_"
                        + sample_name
                        + biny
                    )
                    # print Titley
                    feffy.SetTitle(Titley)
                    feffy.GetYaxis().SetRangeUser(0.0, 1.0)
                    feffy.GetXaxis().SetTitle("eta")
                    feffy.GetYaxis().SetTitle("Efficiency")
                    feffy.SetStats(0)
                    write_outfile(
                        outputfilenamey,
                        feffy,
                        "hist_" + str(sample_name) + "_Eff",
                        bins,
                        tagger,
                        jetcol,
                        wp,
                        flav,
                        biny,
                        sample_name,
                    )
                    del feffy

                del eff
                del h_t
                del h_p
                del h_ref

                del h_pass
        for h in total_histogram + pass_histograms:
            del h

        del h_total_nom

    temp_outputfile.Close()
    os.system("rm " + "temp.root")


def main():
    args = parse_args()

    config = Configuration(args.config)

    create_hists(
        config=config,
        maps_file=args.maps,
        output_tag=args.tag,
        bin_type=args.bin_type,
    )


if __name__ == "__main__":
    main()
