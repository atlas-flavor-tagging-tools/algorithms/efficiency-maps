from itertools import product

import yaml


class Configuration:
    def __init__(self, filepath):
        with open(filepath) as file:
            config = yaml.load(file, Loader=yaml.FullLoader)

        self.samples = config["samples"]
        self.sample_names = list(self.samples.keys())
        self.sample_dsids = [self.samples[key]["dsid"] for key in self.sample_names]
        self.sample_paths = [self.samples[key]["filepath"] for key in self.sample_names]
        self.taggers = config["make_eff"]["taggers"]
        self.flavours = config["make_eff"]["flavours"]
        self.jet_cols = config["make_eff"]["jet_collections"]
        self.map_folder = config["make_eff"]["map_folder"]
        self.working_points = config["make_eff"]["working_points"]
        self.binning = config["binning"]
        self.rename_taggers = config["make_eff"].get("rename_taggers", {})

    def get_hist_names(self, twbins=False, sample_names=False):
        args = [self.jet_cols, self.taggers, self.working_points, self.flavours]
        if twbins:
            args.append(self.binning["twbins"])
        if sample_names:
            args.append(self.sample_names)

        return list(product(*args))

    def get_tagweight_bins(self, tagger):
        twbins = self.binning["twbins"][tagger]
        out = [
            -100,
        ]
        for i in range(1, len(twbins)):
             # Using "-" to split is a terrible idea.
             # The cut on the discriminant can be negative like for GN2v01
            #out.append(float(twbins[i][1:-2].split("-")[0]))
            # let's use ':' instead:
            out.append(float(twbins[i][1:-2].split(":")[0]))
        out.append(100) 
        return out
