import argparse

from configuration import Configuration
from create_eff import create_hists
from create_inputs_AT import create_maps_config
from plot_1d_maps import plot_maps
from plot_2d_maps import plot_2d_maps
from puma.utils import logger


def parse_args():
    """
    Argument parser
    """
    parser = argparse.ArgumentParser(description="Create efficiency maps.")

    parser.add_argument(
        "-c", "--config", required=True, type=str, help="Path to config file."
    )
    parser.add_argument(
        "-b",
        "--bin_type",
        required=False,
        default="c",
        help="Binning type (c - calibrated, f - finely).",
    )
    parser.add_argument(
        "-t", "--tag", required=False, type=str, help="Maps filename tag."
    )
    parser.add_argument(
        "-nw",
        "--no_weight",
        required=False,
        default=False,
        type=bool,
        help="No event weight applied.",
    )

    # possible job options for different steps
    action = parser.add_mutually_exclusive_group(required=True)

    action.add_argument("--prepare", action="store_true", help="Create input maps")

    action.add_argument(
        "--create",
        action="store_true",
        help="Create root files with histograms for efficiency maps",
    )

    action.add_argument("--plot1d", action="store_true", help="Plot efficiency maps")

    action.add_argument("--plot2d", action="store_true", help="Plot 2D efficiency maps")

    action.add_argument("--run_all", action="store_true", help="Run whole pipeline")
    # subparsers = action.add_subparsers()
    # parser_prepare = subparsers.add_parser("")
    args = parser.parse_args()
    return args


def main():
    args = parse_args()

    config = Configuration(args.config)
    if args.prepare:
        logger.info("Creating maps config")
        create_maps_config(config=config, maps_tag=args.tag, noweight=args.no_weight)
    elif args.create:
        logger.info("Creating ROOT histograms")
        create_hists(
            config=config,
            maps_file=f"maps_config_ATofficial{'_no_event_weight' * args.no_weight}_{args.tag}.json",
            output_tag=args.tag,
            bin_type=args.bin_type,
        )
    elif args.plot1d:
        logger.info("Plotting efficiency maps")
        plot_maps(config=config, tag=args.tag, bin_type=args.bin_type)
    elif args.plot2d:
        logger.info("Plotting 2D efficiency maps")
        plot_2d_maps(config=config, tag=args.tag, bin_type=args.bin_type)
    elif args.run_all:
        logger.info("Creating maps config")
        maps_file = create_maps_config(
            config=config, maps_tag=args.tag, noweight=args.no_weight
        )
        logger.info("Creating ROOT histograms")
        create_hists(
            config=config,
            maps_file=maps_file,
            output_tag=args.tag,
            bin_type=args.bin_type,
        )
        logger.info("Plotting efficiency maps")
        plot_maps(config=config, tag=args.tag, bin_type=args.bin_type)
        logger.info("Plotting 2D efficiency maps")
        plot_2d_maps(config=config, tag=args.tag, bin_type=args.bin_type)


if __name__ == "__main__":
    main()
