import argparse

import ROOT
from configuration import Configuration
from helper_functions import makedir
from plotting_tools import create_map, create_map_wp_binned, readdir
from tqdm import tqdm


def parse_args():
    """
    Argument parser
    """
    parser = argparse.ArgumentParser(description="Plot efficiency maps")

    parser.add_argument(
        "-c", "--config", required=True, type=str, help="Path to config file."
    )
    parser.add_argument(
        "-t",
        "--tag",
        required=False,
        default="",
        help="Filename tag",
    )
    parser.add_argument(
        "-b",
        "--bin_type",
        required=False,
        default="c",
        help="Binning type (c - calibrated, f - finely).",
    )

    args = parser.parse_args()
    return args


def plot_maps(config, tag, bin_type):
    maps = {}
    rent = []
    # Select bin type
    if bin_type == "f":
        bins = "finely"
        dir_type = "finely_binned"
    elif bin_type == "c":
        bins = "calibrated"
        dir_type = "calibration_binned"
    else:
        bins = "calibrated"
        dir_type = "calibration_binned"
    # Create directories for efficiency maps
    makedir("vr_eff_maps")
    new_dir_type = makedir("vr_eff_maps", dir_type)
    makedir(new_dir_type, "eff_vs_pt")
    makedir(new_dir_type, "eff_vs_eta")
    # Select eta or pt directory to make maps
    eta_directory = dir_type + "/" + tag + "eff_vs_eta.root"
    pt_directory = dir_type + "/" + tag + "eff_vs_pt.root"
    # Open the directories
    feta = ROOT.TFile.Open(eta_directory)
    fpt = ROOT.TFile.Open(pt_directory)
    ROOT.gROOT.SetBatch(1)  # Run in batch mode
    ROOT.gErrorIgnoreLevel = (
        ROOT.kWarning
    )  # Don't output unnecessary error notifications
    # Create dictionaries for eta and pt containing all the histograms
    heta, _ = readdir(maps, feta, 0, rent)
    hpt, _ = readdir(maps, fpt, 0, rent)
    # Print out total amount of histograms
    print(len(heta) + len(hpt))
    print("-----------------------------")
    # Define directory names
    pt_local = "vr_eff_maps/" + dir_type + "/eff_vs_pt"
    eta_local = "vr_eff_maps/" + dir_type + "/eff_vs_eta"
    wp_local = "vr_eff_maps/" + dir_type + "/eff_vs_wp"

    # Get the current collections, taggers, workingpoints and taggers
    for jetcol, tagger, wp, flav in tqdm(config.get_hist_names(), desc="Main loop"):

        # Choose the binnings for pt and eta
        ptbinning = config.binning[bins][flav]["ptbinning"]
        etabinning = config.binning[bins][flav][
            "etabinning"
        ]  # Choose the binnings for pt and eta

        l_pt = len(ptbinning)  # binning for eta map
        l_eta = len(etabinning)  # binning for pt map

        # Make Eff vs Pt Plots
        for i in tqdm(range(l_eta - 1), leave=False, desc="Eta loop"):
            binx = (
                "eta={" + str(etabinning[i]) + "-" + str(etabinning[i + 1]) + "}"
            )  # Select bins
            if wp == "Continuous":
                twbins = config.binning["twbins"][
                    tagger
                ]  # Choose tag weight bins corresponding to taggers
                create_map_wp_binned(
                    hpt,
                    config.sample_names,
                    bins,
                    tagger,
                    jetcol,
                    wp,
                    flav,
                    binx,
                    twbins,
                    wp_local,
                    config,
                )
                # exit()
                for twbin in twbins:
                    info = [
                        tagger,
                        jetcol,
                        wp,
                        flav,
                        binx,
                        twbin,
                    ]  # Information for maps
                    pathname = "/".join(
                        [bins, tagger, jetcol, wp, flav, binx, twbin]
                    )  # Dictionary key name
                    create_map(
                        hpt,
                        pathname,
                        config.sample_names,
                        info,
                        pt_local,
                        config,
                    )  # Create Efficiency Map
            else:
                info = [
                    tagger,
                    jetcol,
                    wp,
                    flav,
                    binx,
                ]  # Information for maps
                pathname = "/".join(
                    [bins, tagger, jetcol, wp, flav, binx]
                )  # Dictionary key name
                create_map(
                    hpt,
                    pathname,
                    config.sample_names,
                    info,
                    pt_local,
                    config,
                )  # Create Efficiency Map

        # Makre Eff vs Eta Plots
        for j in tqdm(range(l_pt - 1), leave=False, desc="pT loop"):
            biny = "pt={" + str(ptbinning[j]) + "-" + str(ptbinning[j + 1]) + "}"
            if wp == "Continuous":
                for twbin in twbins:
                    info = [
                        tagger,
                        jetcol,
                        wp,
                        flav,
                        binx,
                        twbin,
                    ]  # Information for maps
                    pathname = "/".join(
                        [bins, tagger, jetcol, wp, flav, biny, twbin]
                    )  # Dictionary key name
                    create_map(
                        heta, pathname, config.sample_names, info, eta_local, config
                    )  # Create Efficiency Map
            else:
                info = [
                    tagger,
                    jetcol,
                    wp,
                    flav,
                    biny,
                ]  # Information for maps
                pathname = "/".join(
                    [bins, tagger, jetcol, wp, flav, biny]
                )  # Dictionary key name
                create_map(
                    heta, pathname, config.sample_names, info, eta_local, config
                )  # Create Efficiency Map

    feta.Close()
    fpt.Close()


def main():
    args = parse_args()

    config = Configuration(args.config)

    plot_maps(config, args.tag, args.bin_type)


if __name__ == "__main__":
    main()
