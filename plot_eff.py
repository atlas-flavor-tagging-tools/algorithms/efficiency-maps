"""Efficiency plots vs. specific variable."""
import matplotlib as mpl
import numpy as np
from matplotlib import axis, gridspec, lines
from matplotlib.figure import Figure

# TODO: fix the import below
from puma.plot_base import PlotBase, PlotLineObject
from puma.utils import get_good_colours, logger, set_xaxis_ticklabels_invisible
from puma.utils.histogram import hist_ratio


class VarVsEff(PlotLineObject):  # pylint: disable=too-many-instance-attributes
    """
    var_vs_eff class storing info about curve and allows to calculate ratio w.r.t other
    efficiency plots.
    """

    def __init__(  # pylint: disable=too-many-arguments
        self,
        bins: np.ndarray,
        eff: np.ndarray,
        error: np.ndarray,
        key: str = None,
        **kwargs,
    ) -> None:
        """Initialise properties of roc curve object.

        Parameters
        ----------
        x_var : np.ndarray
            Values for x-axis variable for eff
        y_val : np.ndarray
            Values of eff
        bins : int or sequence of scalars, optional
            If bins is an int, it defines the number of equal-width bins in the
            given range (10, by default). If bins is a sequence, it defines a
            monotonically increasing array of bin edges, including the
            rightmost edge, allowing for non-uniform bin widths, by default 10
        key : str, optional
            Identifier for the curve e.g. tagger, by default None
        **kwargs : kwargs
            Keyword arguments passed to `PlotLineObject`

        Raises
        ------
        ValueError
            If provided options are not compatible with each other
        """
        # TODO: in python 3.10 add multipe type operator | for bins and disc_cut

        super().__init__(**kwargs)
        self.bins = np.array(bins)
        self.eff = np.array(eff)
        self.error = np.array(error)
        self.key = key
        # Binning related variables
        self.n_bins = None
        self.bn_edges = None
        self.x_bin_centres = None
        self.bin_widths = None
        self.n_bins = None

        self._set_bin_edges(bins)
        self.inverse_cut = False

    def _set_bin_edges(self, bins):
        """Calculate bin edges, centres and width and save them as class variables.

        Parameters
        ----------
        bins : int or sequence of scalars
            If bins is an int, it defines the number of equal-width bins in the given
            range. If bins is a sequence, it defines a monotonically increasing array of
            bin edges, including the rightmost edge, allowing for non-uniform bin
            widths.
        """
        self.bin_edges = np.array(bins)
        logger.debug("Retrieved bin edges %s}", self.bin_edges)
        # Get the bins for the histogram
        self.x_bin_centres = (self.bin_edges[:-1] + self.bin_edges[1:]) / 2.0
        self.bin_widths = (self.bin_edges[1:] - self.bin_edges[:-1]) / 2.0
        self.n_bins = self.bin_edges.size - 1
        logger.debug("N bins: %i", self.n_bins)

    def divide(self, other):
        """Calculate ratio between two class objects.

        Parameters
        ----------
        other : var_vs_eff class
            Second var_vs_eff object to calculate ratio with
        mode : str
            Defines the mode which is used for the ratoi calculation, can be the
            following values: `sig_eff`, `bkg_eff`, `sig_rej`, `bkg_rej`
        inverse : bool
            If False the ratio is calculated `this / other`,
            if True the inverse is calculated
        inverse_cut : bool
            Inverts the discriminant cut, which will yield the efficiency or rejection
            of the jets not passing the working point, by default False

        Returns
        -------
        np.ndarray
            Ratio
        np.ndarray
            Ratio error
        np.ndarray
            Bin centres
        np.ndarray
            Bin widths

        Raises
        ------
        ValueError
            If binning is not identical between 2 objects
        """
        if not np.array_equal(self.bin_edges, other.bin_edges):
            raise ValueError("The binning of the two given objects do not match.")
        # TODO: python 3.10 switch to cases syntax
        nom, nom_err = self.eff, self.error
        denom, denom_err = other.eff, other.error

        ratio, ratio_err = hist_ratio(
            nom,
            denom,
            nom_err,
            denom_err,
            step=False,
        )
        return (
            ratio,
            ratio_err,
            self.x_bin_centres,
            self.bin_widths,
        )


class VarVsEffPlot(PlotBase):  # pylint: disable=too-many-instance-attributes
    """var_vs_eff plot class"""

    def __init__(self, **kwargs) -> None:
        """var_vs_eff plot properties

        Parameters
        ----------
        mode : str
            Defines which quantity is plotted, the following options ar available:
            "sig_eff", "bkg_eff", "sig_rej" or "bkg_rej"
        **kwargs : kwargs
            Keyword arguments from `puma.PlotObject`

        Raises
        ------
        ValueError
            If incompatible mode given or more than 1 ratio panel requested
        """
        super().__init__(**kwargs)
        self.plot_objects = {}
        self.add_order = []
        self.ratios_objects = {}
        self.ratio_axes = {}
        self.reference_object = None
        self.bin_edge_min = np.inf
        self.bin_edge_max = -np.inf
        self.inverse_cut = False
        if self.n_ratio_panels > 1:
            raise ValueError("Not more than one ratio panel supported.")
        self.initialise_figure()

    def initialise_figure(self):
        """
        Initialising matplotlib.figure.Figure for different scenarios depending on how
        many ratio panels are requested.
        """
        # TODO: switch to cases syntax in python 3.10

        # you must use increments of 0.1 for the deminsions
        width = 5.0
        top_height = 2.7 if self.n_ratio_panels else 3.5
        ratio_height = 1.5
        height = top_height + self.n_ratio_panels * ratio_height
        figsize = (width, height) if self.figsize is None else self.figsize
        self.fig = Figure(figsize=figsize, layout="constrained")

        if self.n_ratio_panels == 0:
            self.axis_top = self.fig.gca()
        elif self.n_ratio_panels > 0:
            g_spec_height = (top_height + ratio_height * self.n_ratio_panels) * 10
            g_spec = gridspec.GridSpec(int(g_spec_height), 1, figure=self.fig)
            self.axis_top = self.fig.add_subplot(g_spec[: int(top_height * 10), 0])
            set_xaxis_ticklabels_invisible(self.axis_top)
            for i in range(1, self.n_ratio_panels + 1):
                start = int((top_height + ratio_height * (i - 1)) * 10)
                stop = int(start + ratio_height * 10)
                sub_axis = self.fig.add_subplot(
                    g_spec[start:stop, 0], sharex=self.axis_top
                )
                if i < self.n_ratio_panels:
                    set_xaxis_ticklabels_invisible(sub_axis)
                setattr(self, f"axis_ratio_{i}", sub_axis)

        """if self.grid:
            self.axis_top.grid(lw=0.3)
            if self.axis_ratio_1:
                self.axis_ratio_1.grid(lw=0.3)
            if self.axis_ratio_2:
                self.axis_ratio_2.grid(lw=0.3)"""

    def add(self, curve: object, key: str = None, reference: bool = False):
        """Adding var_vs_eff object to figure.

        Parameters
        ----------
        curve : var_vs_eff class
            Var_vs_eff curve
        key : str, optional
            Unique identifier for var_vs_eff, by default None
        reference : bool, optional
            If var_vs_eff is used as reference for ratio calculation, by default False

        Raises
        ------
        KeyError
            If unique identifier key is used twice
        """
        if key is None:
            key = len(self.plot_objects) + 1
        if key in self.plot_objects:
            raise KeyError(f"Duplicated key {key} already used for unique identifier.")

        self.plot_objects[key] = curve
        self.add_order.append(key)
        # set linestyle
        if curve.linestyle is None:
            curve.linestyle = "-"
        # set colours
        if curve.colour is None:
            curve.colour = get_good_colours()[len(self.plot_objects) - 1]
        # set alpha
        if curve.alpha is None:
            curve.alpha = 0.8
        # set linewidth
        if curve.linewidth is None:
            curve.linewidth = 1.6

        # set min and max bin edges
        self.bin_edge_min = min(self.bin_edge_min, curve.bin_edges[0])
        self.bin_edge_max = max(self.bin_edge_max, curve.bin_edges[-1])

        if reference:
            logger.debug("Setting roc %s as reference.", key)
            self.set_reference(key)

    def set_reference(self, key: str):
        """Setting the reference roc curves used in the ratios

        Parameters
        ----------
        key : str
            Unique identifier of roc object
        """
        if self.reference_object is None:
            self.reference_object = key
        else:
            logger.warning(
                "You specified a second curve %s as reference for ratio. "
                "Using it as new reference instead of %s.",
                key,
                self.reference_object,
            )
            self.reference_object = key

    def plot(self, **kwargs):
        """Plotting curves

        Parameters
        ----------
        **kwargs: kwargs
            Keyword arguments passed to plt.axis.errorbar

        Returns
        -------
        Line2D
            matplotlib Line2D object
        """
        logger.debug("Plotting curves")
        plt_handles = []
        for key in self.add_order:
            elem = self.plot_objects[key]
            y_value, y_error = elem.eff, elem.error
            error_bar = self.axis_top.errorbar(
                elem.x_bin_centres,
                y_value,
                xerr=elem.bin_widths,
                yerr=np.zeros(elem.n_bins),
                color=elem.colour,
                fmt="none",
                label=elem.label,
                alpha=elem.alpha,
                linewidth=elem.linewidth,
                **kwargs,
            )
            # set linestyle for errorbar
            error_bar[-1][0].set_linestyle(elem.linestyle)
            down_variation = y_value - y_error
            up_variation = y_value + y_error
            down_variation = np.concatenate((down_variation[:1], down_variation[:]))
            up_variation = np.concatenate((up_variation[:1], up_variation[:]))

            # draw markers
            self.axis_top.scatter(
                x=elem.x_bin_centres,
                y=y_value,
                marker=elem.marker,
                color=elem.colour,
            )

            # fill uncertanty
            self.axis_top.fill_between(
                elem.bin_edges,
                down_variation,
                up_variation,
                color=elem.colour,
                alpha=0.3,
                zorder=1,
                step="pre",
                edgecolor="none",
                linestyle=elem.linestyle,
            )
            plt_handles.append(
                mpl.lines.Line2D(
                    [],
                    [],
                    color=elem.colour,
                    label=elem.label,
                    linestyle=elem.linestyle,
                    marker=elem.marker,
                )
            )
        return plt_handles

    def plot_ratios(self):
        """Plotting ratio curves.

        Raises
        ------
        ValueError
            If no reference curve is defined
        """
        if self.reference_object is None:
            raise ValueError("Please specify a reference curve.")
        for key in self.add_order:
            elem = self.plot_objects[key]
            (
                ratio,
                ratio_err,
                x_bin_centres,
                bin_widths,
            ) = elem.divide(self.plot_objects[self.reference_object])
            error_bar = self.axis_ratio_1.errorbar(
                x_bin_centres,
                ratio,
                xerr=bin_widths,
                yerr=np.zeros(elem.n_bins),
                color=elem.colour,
                fmt="none",
                alpha=elem.alpha,
                linewidth=elem.linewidth,
            )
            # set linestyle for errorbar
            error_bar[-1][0].set_linestyle(elem.linestyle)
            down_variation = ratio - ratio_err
            up_variation = ratio + ratio_err
            down_variation = np.concatenate((down_variation[:1], down_variation[:]))
            up_variation = np.concatenate((up_variation[:1], up_variation[:]))

            # draw markers
            self.axis_ratio_1.scatter(
                x=elem.x_bin_centres, y=ratio, marker=elem.marker, color=elem.colour
            )
            self.axis_ratio_1.fill_between(
                elem.bin_edges,
                down_variation,
                up_variation,
                color=elem.colour,
                alpha=0.3,
                zorder=1,
                step="pre",
                edgecolor="none",
            )

    def set_grid(self):
        """Set gtid lines."""
        self.axis_top.grid()
        self.axis_ratio_1.grid()

    def set_inverse_cut(self, inverse_cut=True):
        """Invert the discriminant cut, which will yield the efficiency or rejection
        of the jets not passing the working point.

        Parameters
        ----------
        inverse_cut : bool, optional
            Invert discriminant cut, by default True
        """
        self.inverse_cut = inverse_cut

    def draw_hline(self, y_val: float):
        """Draw hline in top plot panel.

        Parameters
        ----------
        y_val : float
            y value of the horizontal line
        """
        self.axis_top.hlines(
            y=y_val,
            xmin=self.bin_edge_min,
            xmax=self.bin_edge_max,
            colors="black",
            linestyle="dotted",
            alpha=0.5,
        )

    def draw(self, labelpad: int = None, xlables: list = None):
        """Draw figure.

        Parameters
        ----------
        labelpad : int, optional
            Spacing in points from the axes bounding box including
            ticks and tick labels, by default "ratio"
        """
        self.set_xlim(
            self.bin_edge_min if self.xmin is None else self.xmin,
            self.bin_edge_max if self.xmax is None else self.xmax,
        )
        plt_handles = self.plot()
        if self.n_ratio_panels == 1:
            self.plot_ratios()
        self.set_title()
        self.set_log()
        self.set_y_lim()
        #self.set_xlabel()
        self.set_tick_params()
        self.set_ylabel(self.axis_top)
        if xlables:
            self.axis_top.set_xticks(np.arange(len(xlables)) + 0.5)
            self.axis_top.set_xticklabels(xlables)
        """if self.n_ratio_panels > 0:
            self.set_ylabel(
                self.axis_ratio_1,
                self.ylabel_ratio_1,
                align_right=False,
                labelpad=labelpad,
            )"""
        self.make_legend(plt_handles, ax_mpl=self.axis_top)
        # if not self.atlas_tag_outside:
        #     self.tight_layout()
        self.plotting_done = True
        if self.apply_atlas_style is True:
            self.atlasify(use_tag=self.use_atlas_tag)
