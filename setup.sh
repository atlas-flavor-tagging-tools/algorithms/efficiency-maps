#!/bin/bash

# set up ROOT and python virtualenv
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh -q
lsetup "root 6.28.10-x86_64-el9-gcc13-opt"

# setup python virtualenv
DIR="venv/"
if [ -d "$DIR" ]; then
  echo "Using python virtual environment in ${DIR}..."
else
  echo "Info: ${DIR} not found. Setting up python virtual environment for the first time..."
  python3 -m venv ${DIR}
  echo "Python virtual environment set up! Activating it now..."
fi

# activate virtual environment (to allow for installation of packages)
source venv/bin/activate

# install required packages
pip install -r requirements.txt
