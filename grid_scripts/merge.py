import ROOT
import os
import sys
import numpy as np

ROOT.TH1.SetDefaultSumw2()
ROOT.TH2.SetDefaultSumw2()
#processes = {"":"",
#             "":"",
#             "":""}

# Complete this list of find a way to extract this info from histograms
#jets_reconstruction_algorithms = ["AntiKt4EMPFlowJets","AntiKtVR30Rmax4Rmin02PV0TrackJets"] 

# Function that adds all histogram within a list and returns the result
def sum_hist_list(hist_list):
    #print(hist_list)
    hist_output = hist_list[0].Clone()
    for hist in hist_list[1:]:
        hist_output.Add(hist)
    return hist_output

# Function that scale a 2D histogram but keeps the error content of each bin the same
def Scale_hist_const_err(hist_2D,scale_factor):
    # Number of bins for the histogram in both dimensions
    nx = hist_2D.GetNbinsX()
    ny = hist_2D.GetNbinsY()
    
    # The array that holds the erros for each bin  
    errs = np.zeros((nx,ny))
    for x in range(1,nx):
        for y in range(1,ny):
            errs[x,y] = hist_2D.GetBinError(x,y)
    
    # Scale the histogram 
    hist_2D.Scale(scale_factor)

    # Now we set the errors back to what they were pre-scaling
    for x in range(1,nx):
        for y in range(1,ny):
            hist_2D.SetBinError(x,y,errs[x,y])

def get_cross_section(XSection_file_path,DSID):
    cross_section = 0
    with open(XSection_file_path) as f:
        for line in f.readlines():
            line_stripped = line.replace("\t"," ").strip()
            line_split = line_stripped.split(" ")
            try:
                if int(line_split[0]) == DSID:
                    while '' in line_split:
                        line_split.remove('')
                    #print(line_split)
                    cross_section = float(line_split[1]) # in pb
            except:
                pass
    return cross_section

# Assumed constants:
target_luminosity = 3000 * 1000 # pb^-1 , choice of luminosity should not matter since we are interested in ratios later on

# Parameters -----------------------------------------------------------------
# File used by TopReco to get the cross sections
xsection_filepath1 = "/eos/user/t/thmaurin/grid/efficiency_map_ftag/grid_scripts/XSection-MC16-13TeV.data"
xsection_filepath2 = "/eos/user/t/thmaurin/grid/efficiency_map_ftag/grid_scripts/XSection-MC21-13p6TeV.data" 
# path to the directory where all the files are downloaded in directories
if len(sys.argv) < 2:
    sys.exit("No path supplied to the folder containing the outputs to merge.")
working_dir = sys.argv[1]
#-----------------------------------------------------------------------------

ifiles = [] # We need to keep a reference to the files otherwise read histograms gets deleted
scaled_hists = {} # Dictionary that has: (key = histogram name, values = rescaled histogram object)
# Assume all folders of downloaded root files are in the same directory 
for folder_name in os.listdir(working_dir):
    folder_path = working_dir + "/"+folder_name
    # First we skip every file/folder that does not have "user." at the front. These are not the folder we downloaded.
    if not folder_name.startswith("user."):
        continue

    # Getting the information from the file names:
    DSID = folder_name.split(".")[3] # Can it be in a different place?
    DSID = int(DSID)

    # Getting the cross section
    cross_section = get_cross_section(xsection_filepath1,DSID)
    if cross_section == 0: # If first file doesn't work try second file
        cross_section = get_cross_section(xsection_filepath2,DSID)
    if cross_section == 0:
        print(" ERROR : Cross section of 0 !!! ")

    print("Cross section for ", DSID,":",cross_section,".")

    # Extract the physics histograms and scale them
    # Assume that a merged file beginning with "merged_" is produced for all root files in each folder.
    try:
        root_file_name = [file_name for file_name in list(os.listdir(folder_path)) if file_name.startswith("merged_")][0]
        
    except:
        print("There is no file beginning with 'merged_' in ",folder_path)
        continue
    
    print("Root file selected :",root_file_name)
    
    # Open the root file and get the ftag directory
    ifile = ROOT.TFile(folder_path+"/"+root_file_name,"READ")
    ifiles.append(ifile)
    ftag_directory = ifile.Get("ftag")
    jetFtagPlot_directory = ftag_directory.Get("JetFtagEffPlots")

    # We need the sum of weights to normalise the histograms:
    # no_event_weighted histograms are weighted by sum of events instead
    sumW = 0
    sumW_no_event_weight = 0
    for event in ifile.sumWeights:
        sumW += event.totalEventsWeighted
        sumW_no_event_weight += event.totalEvents

    # Iterate over the histograms in the directory
    hist_name_list = jetFtagPlot_directory.GetListOfKeys()
    for key in hist_name_list:
        hist_name = key.GetName()
        
        # Get the histogram
        hist = jetFtagPlot_directory.Get(hist_name)
        
        # Rescale it using luminosity:
        # Compute the current luminosity using the number of events and the cross section gathered earlier
        if "no_event_weight" in hist_name:
            current_luminosity = sumW_no_event_weight/cross_section 
        else:
            current_luminosity = sumW/cross_section 
        # rescale the histogram to the new luminosity, this is how the cross section is taken into account
        hist.Scale(target_luminosity/current_luminosity) 
        #hist.Scale(cross_section/sumW) # this is equivalent to choosing a target luminosity of 1 pb^-1 
        #Scale_hist_const_err(hist,cross_section/sumW)

        # Make sure that a list exists for each histogram name
        if hist_name not in scaled_hists:
            scaled_hists[hist_name] = []

        # Append the histogram to our dictionary of lists
        scaled_hists[hist_name].append(hist)
    
# Once we have all the rescaled histogram, we add them up and write them to a new root file:
# Create the new root file, make the ftag directory and cd to it
new_ifile = ROOT.TFile(working_dir+"/final_merged.root","RECREATE") # Maybe find a clean way to add the generator in the filename
new_ftag_directory = new_ifile.mkdir("ftag")
new_ftag_directory.cd()
new_ftag_jetFtagPlot_directory = new_ftag_directory.mkdir("JetFtagEffPlots")
new_ftag_jetFtagPlot_directory.cd()
for hist_name in scaled_hists:
    summed_hist = sum_hist_list(scaled_hists[hist_name])
    summed_hist.Write()

new_ifile.Close()
ifile.Close()


