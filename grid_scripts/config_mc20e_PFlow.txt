LibraryNames libTopEventSelectionTools libTopEventReconstructionTools

#NEvents 1000

### Good Run List
GRLDir  GoodRunsLists
GRLFile data15_13TeV/20170619/physics_25ns_21.0.19.xml data16_13TeV/20180129/physics_25ns_21.0.19.xml data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.xml data18_13TeV/20190318/physics_25ns_Triggerno17e33prim.xml

BTagCDIPath xAODBTaggingEfficiency/13TeV/2022-22-13TeV-MC20-CDI-2022-07-28_v1.root

ForceRandomRunNumber 310000

# showering algorithm
TDPPath XSection-MC16-13TeV.data
ElectronTriggerEfficiencyConfig SINGLE_E_2022_e26_lhtight_ivarloose_L1EM22VHI_OR_e60_lhmedium_L1EM22VHI_OR_e140_lhloose_L1EM22VHI_OR_HLT_e300_etcut_L1EM22VHI

ElectronCollectionName None #Electrons
MuonCollectionName Muons
JetCollectionName AntiKt4EMPFlowJets#_BTagging201903 #AntiKt4EMTopoJets
LargeJetCollectionName None
LargeJetSubstructure None
TauCollectionName None
PhotonCollectionName None

TrackJetCollectionName None

TruthCollectionName None
TruthJetCollectionName None
TopPartonHistory False
TopParticleLevel False
TruthBlockInfo False
PDFInfo False

TruthElectronCollectionName TruthElectrons
TruthMuonCollectionName None
TruthJetCollectionName None

ObjectSelectionName top::ObjectLoaderStandardCuts
OutputFormat top::EventSaverFlatNtuple
OutputEvents SelectedEvents
OutputFilename output.root
PerfStats No

Systematics None
JetUncertainties_NPModel CategoryReduction
	
MCGeneratorWeights Nominal

JetPt 5000
JetEta 2.5
TrackJetPt 5000
TrackJetEta 2.5
BTaggingCaloJetWP DL1dv01:FixedCutBEff_60 DL1dv01:FixedCutBEff_70 DL1dv01:FixedCutBEff_77 DL1dv01:FixedCutBEff_85 DL1dv01:Continuous DL1dv00:FixedCutBEff_60 DL1dv00:FixedCutBEff_70 DL1dv00:FixedCutBEff_77 DL1dv00:FixedCutBEff_85 DL1dv00:Continuous
BTaggingCaloJetUncalibWP GN120220509:FixedCutBEff_60 GN120220509:FixedCutBEff_70 GN120220509:FixedCutBEff_77 GN120220509:FixedCutBEff_85 GN120220509:Continuous
#BTagVariableSaveList None


DoTight MC
DoLoose False
ElectronIsolation Tight_VarRad
ElectronIsolationSF None
IsAFII False
FilterBranches *

########################
### basic selection with mandatory cuts for reco level
########################

SUB BASIC
INITIAL
GRL
GOODCALO
PRIVTX
RECO_LEVEL
TRACKJETCLEAN

#########################
### FTag efficiency maps histograms
########################

SUB FTAG_EffPlots
JETFTAGEFFPLOTS fill_total_hist WP:FixedCutBEff_60 tagger:DL1dv01  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2022-22-13TeV-MC20-CDI-2022-07-28_v1.root dont_use_event_weight suffix:_no_event_weight
JETFTAGEFFPLOTS WP:FixedCutBEff_70 tagger:DL1dv01  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2022-22-13TeV-MC20-CDI-2022-07-28_v1.root dont_use_event_weight suffix:_no_event_weight 
JETFTAGEFFPLOTS WP:FixedCutBEff_77 tagger:DL1dv01  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2022-22-13TeV-MC20-CDI-2022-07-28_v1.root dont_use_event_weight suffix:_no_event_weight 
JETFTAGEFFPLOTS WP:FixedCutBEff_85 tagger:DL1dv01  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2022-22-13TeV-MC20-CDI-2022-07-28_v1.root dont_use_event_weight suffix:_no_event_weight 
JETFTAGEFFPLOTS WP:FixedCutBEff_60 tagger:DL1dv00  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2022-22-13TeV-MC20-CDI-2022-07-28_v1.root dont_use_event_weight suffix:_no_event_weight 
JETFTAGEFFPLOTS WP:FixedCutBEff_70 tagger:DL1dv00  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2022-22-13TeV-MC20-CDI-2022-07-28_v1.root dont_use_event_weight suffix:_no_event_weight 
JETFTAGEFFPLOTS WP:FixedCutBEff_77 tagger:DL1dv00  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2022-22-13TeV-MC20-CDI-2022-07-28_v1.root dont_use_event_weight suffix:_no_event_weight 
JETFTAGEFFPLOTS WP:FixedCutBEff_85 tagger:DL1dv00  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2022-22-13TeV-MC20-CDI-2022-07-28_v1.root dont_use_event_weight suffix:_no_event_weight
JETFTAGEFFPLOTS WP:FixedCutBEff_60 tagger:GN120220509  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2022-22-13TeV-MC20-CDI-2022-07-28_v1.root dont_use_event_weight suffix:_no_event_weight 
JETFTAGEFFPLOTS WP:FixedCutBEff_70 tagger:GN120220509  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2022-22-13TeV-MC20-CDI-2022-07-28_v1.root dont_use_event_weight suffix:_no_event_weight 
JETFTAGEFFPLOTS WP:FixedCutBEff_77 tagger:GN120220509  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2022-22-13TeV-MC20-CDI-2022-07-28_v1.root dont_use_event_weight suffix:_no_event_weight 
JETFTAGEFFPLOTS WP:FixedCutBEff_85 tagger:GN120220509  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2022-22-13TeV-MC20-CDI-2022-07-28_v1.root dont_use_event_weight suffix:_no_event_weight 



JETFTAGEFFPLOTS fill_total_hist WP:FixedCutBEff_60 tagger:DL1dv01  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2022-22-13TeV-MC20-CDI-2022-07-28_v1.root
JETFTAGEFFPLOTS WP:FixedCutBEff_70 tagger:DL1dv01  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2022-22-13TeV-MC20-CDI-2022-07-28_v1.root
JETFTAGEFFPLOTS WP:FixedCutBEff_77 tagger:DL1dv01  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2022-22-13TeV-MC20-CDI-2022-07-28_v1.root
JETFTAGEFFPLOTS WP:FixedCutBEff_85 tagger:DL1dv01  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2022-22-13TeV-MC20-CDI-2022-07-28_v1.root
JETFTAGEFFPLOTS WP:FixedCutBEff_60 tagger:DL1dv00  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2022-22-13TeV-MC20-CDI-2022-07-28_v1.root
JETFTAGEFFPLOTS WP:FixedCutBEff_70 tagger:DL1dv00  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2022-22-13TeV-MC20-CDI-2022-07-28_v1.root
JETFTAGEFFPLOTS WP:FixedCutBEff_77 tagger:DL1dv00  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2022-22-13TeV-MC20-CDI-2022-07-28_v1.root
JETFTAGEFFPLOTS WP:FixedCutBEff_85 tagger:DL1dv00  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2022-22-13TeV-MC20-CDI-2022-07-28_v1.root
JETFTAGEFFPLOTS WP:FixedCutBEff_60 tagger:GN120220509  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2022-22-13TeV-MC20-CDI-2022-07-28_v1.root
JETFTAGEFFPLOTS WP:FixedCutBEff_70 tagger:GN120220509  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2022-22-13TeV-MC20-CDI-2022-07-28_v1.root
JETFTAGEFFPLOTS WP:FixedCutBEff_77 tagger:GN120220509  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2022-22-13TeV-MC20-CDI-2022-07-28_v1.root
JETFTAGEFFPLOTS WP:FixedCutBEff_85 tagger:GN120220509  N_pT:3000 min_pT:0 max_pT:3000 N_eta:60 min_eta:0 max_eta:3 CDIfile:xAODBTaggingEfficiency/13TeV/2022-22-13TeV-MC20-CDI-2022-07-28_v1.root


########################
### inclusive selection
########################
SUB ftag_basic
. BASIC

#JETCLEAN LooseBad


SELECTION ftag
. ftag_basic
. FTAG_EffPlots
SAVE
