export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh -q
lsetup "rucio -w"
voms-proxy-init -voms atlas
asetup AnalysisBase,24.2.7
lsetup panda
