import os
import yaml
import argparse
import re


def GetParser():
    parser = argparse.ArgumentParser(description="Grid submission of JetFtagEffPlots")

    parser.add_argument(
        "-c",
        "--config",
        type=str,
        required=True,
        help="Path to the submission config",
    )

    parser.add_argument("-t", "--tag", type=str, help="Overwrite file tag")
    return parser.parse_args()

def refine_oDS(oDS):
    
    oDS_output = oDS
    while len(oDS_output) > 115:
        # print len(oDS)," too long!!!"
        splODS = oDS_output.split("_")
        splODS.pop(2)
        oDS_output = "_".join(splODS)
        oDS_output.replace(" ", "")
    
    return oDS_output

# Grid submission 
def submitJobCommand(ds, config_file,extFile,oDS,nFilesPerJob):
    com = f'prun --useAthenaPackages --inDS {ds} --outputs=output.root --writeInputToTxt=%IN:in.txt --exec="top-xaod {config_file} in.txt" --extFile={extFile} --outDS {oDS} --mergeOutput'
    if nFilesPerJob != None:
        com += f" --nFilesPerJob={nFilesPerJob}"
    return com

def downloadOutputCommand(oDS,download_directory = "./"):
    com = f'rucio download --dir ./{download_directory} {oDS}_output.root'
    return com

def mergeOutputCommands(oDS,download_directory = "./"):
    com1 = f'cd {download_directory}/{oDS}_output.root'
    new_name = "merged_" + '.'.join(oDS.split('.')[2:]) + ".root"
    com2 = f'hadd {new_name} *.root'
    com3 = 'cd -' 
    return com1,com2,com3

def mergeXSectionCommands(merge_python_script_path,download_directories):
    coms = []
    for directory in download_directories:
        coms.append(f'python3 {merge_python_script_path} {directory}')
    return coms

def main():

    args = GetParser()
    with open(args.config, "r") as file:
        config = yaml.safe_load(file)
    if args.tag:
        config["tag"] = args.tag
    
    
    download_merge_script_filename = "download_merge_" + config['tag'] + ".sh"
    download_merge_script = open(download_merge_script_filename,"w+") # open the file that will be our download + merge script
    #CDIfile = "xAODBTaggingEfficiency/13TeV/2022-22-13TeV-MC20-CDI-2022-07-28_v1.root"
    CDIfile = config["CDIfile"]
    
    try :
        nFilesPerJob = config["nFilesPerJob"]
    except:
        print("No nFilesPerJob specified. (grid default settings will be used)")
        nFilesPerJob = None

    print("nFilesPerJob =", nFilesPerJob)

    # This variable olds the absolute path to the merging python script to run on the output during merging
    merge_python_script_path = os.path.abspath(os.path.dirname(__file__))+"/merge.py"
    #print('merge_python_script_path ==',merge_python_script_path)
    # download_directory is a variable that holds the name of the directory where files will be downloaded to
    download_directory = "download_directory"
    download_directories = [] # Keeps track of all the directories created for the outputs
    for sample in config["samples"]:
        ##########################################################################################################################
        suffix = f".{sample['period']}.{config['tag']}"  # ".pflow.v0"
        username = config["username"]  # os.getenv("USER")
        inputlist = sample["sample_list"]  # "sample_list_beam_spot.txt"
        config_file = sample["config_file"]  #'config_mc16e_Pflow.txt'
        if "prw_file" in sample:
            prw_file = sample["prw_file"]
            extFile = f"{CDIfile},{prw_file}"
        else:
            prw_file = None
            extFile = str(CDIfile)
        ##########################################################################################################################
        # Fix the PRW file in config_file if needed
        if prw_file is not None:
            with open(config_file, "r") as f:
                file_data = f.read()
            file_data = re.sub(
                r"(^PRWConfigFiles_\w{2}\s*)(.*\w*)",
                r"\1" + prw_file,
                file_data,
                flags=re.M,
            )
            with open(config_file, "w") as f:
                f.write(file_data)
        ###########################################################################################################################
        dsList = open(inputlist, "r")
        lines = dsList.readlines()
        
        ############################################################################################################################################
        ############################################################################################################################################
        # Identify the mc files in the sample lists
        for line in lines:
            line = line.strip() # removes the \n and spaces at the end of the string
            if "#" in line:
                continue
            if line.startswith(">") or line.startswith(" >"):
                download_directory = line.split(">")[1].strip() # Get the directory name where the outputs are downloaded
                # If the download directory name is not saved, then add it to the list to specify it for the XSection merging script
                if download_directory not in download_directories:
                    download_directories.append(download_directory)
                continue
            if (
                "mc" not in line
                and "valid" not in line
                and "group" not in line
                and "data" not in line
            ):
                continue
            # print "///////////////////////////////////////////////////////////////////////////////////////"
            oDS = "user." + username + "." + line.replace("/", "") + suffix
            print("DATASET: ",line)
            print()
            oDS = refine_oDS(oDS) # We need to refine the oDS due to length limit
            command = submitJobCommand(line, config_file,extFile,oDS,nFilesPerJob)
            print(command)
            os.system(command) # Execute submitting command
            
            # Write commands in the download_merge_script for each sample file
            download_merge_script.write(downloadOutputCommand(oDS,download_directory)+"\n")
            for com in mergeOutputCommands(oDS,download_directory):
                download_merge_script.write(com+"\n")
    # At the end of the script, add lines to run the XSection merging script on all download directories.
    for com in mergeXSectionCommands(merge_python_script_path,download_directories):
        download_merge_script.write(com+"\n")

    download_merge_script.close() # Close the file!
    print()
    print("The file ",download_merge_script_filename," has been created to download and merge the jobs' outputs once they are done.")

if __name__ == "__main__":
    main()
