import sys

import ROOT
from get_hist import validation_rebin, validation_rebin_3D
from plotting_tools import readdir

ROOT.gROOT.SetBatch(1)

filename = sys.argv[1]

file = ROOT.TFile.Open(filename)

k = 0
maps = {}
rent = []

hist_list, name_list = readdir(maps, file, k, rent)

for name in name_list:
    if "Continuous" in name:
        validation_rebin_3D(hist_list[name], name)
    else:
        validation_rebin(hist_list[name], name)


file.Close()
