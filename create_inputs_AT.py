import argparse
import json

from configuration import Configuration


def parse_args():
    """
    Argument parser
    """
    parser = argparse.ArgumentParser(description="Create eff maps configs.")

    parser.add_argument(
        "-c", "--config", required=True, type=str, help="Path to config file."
    )
    parser.add_argument(
        "-t", "--tag", required=False, type=str, help="Maps filename tag"
    )
    parser.add_argument(
        "-nw",
        "--no_weight",
        required=False,
        default=False,
        help="No event weight applied",
    )

    args = parser.parse_args()
    return args


def create_maps_config(config, maps_tag=None, noweight=False):

    wr = 0
    noweight = False

    histogram_dir = ""
    if noweight:
        maps_filename = f"maps_config_ATofficial_no_event_weight_{maps_tag}.json"
    else:
        maps_filename = f"maps_config_ATofficial_{maps_tag}.json"

    # datasets = data_extract()

    map_configs = {}

    job_index = -1

    for jetcol, tagger, wp, flav in config.get_hist_names():

        job_index += 1

        map_name = jetcol + "_" + tagger + "_" + wp + "_" + flav
        # sample_names = list(datasets.keys())
        # hadronisations = [item[1] for item in list(sample_ids.values())]

        map_config = {}
        map_config["map_folder"] = config.map_folder
        map_config["flav"] = flav
        map_config["jetcol"] = jetcol
        map_config["tagger"] = tagger
        map_config["wp"] = wp
        map_config["sample_dsid"] = config.sample_dsids
        map_config["sample_names"] = config.sample_names

        # Added this to remove hardcoding...
        config_working_points = [wp for wp in config.working_points if "Continuous" not in wp]
        config_working_points = config_working_points[::-1] # invert list to match stuff before

        total_hists = {}
        passed_hists = {}

        # for sample_name in input_samples:
        for sample_i, (sample_name, sample_id) in enumerate(
            zip(map_config["sample_names"], map_config["sample_dsid"])
        ):
            # print sample_name
            if wp == "Continuous":
                infile = histogram_dir + config.samples[sample_name]["filepath"]
                total_hists[sample_name] =  [[] for i in range(len(config_working_points))]
                passed_hists[sample_name] = [[] for i in range(len(config_working_points))]
                
                for cont_bin, wp_i in enumerate(
                    #["FixedCutBEff_" + x for x in ["85", "77", "70", "60"]] # Hard coded wp :(
                    config_working_points # obtained from the config file here instead
                ):
                    total_hist = f"ftag/{config.map_folder}/{flav}_total_{jetcol}"
                    pass_hist = (
                        f"ftag/{config.map_folder}/{tagger}_{wp_i}_{jetcol}_{flav}_pass"
                    )

                    total_hists[sample_name][cont_bin].append([infile, total_hist])
                    passed_hists[sample_name][cont_bin].append([infile, pass_hist])

                map_config["total_hists"] = total_hists
                map_config["passed_hists"] = passed_hists
                wr += 1
            else:
                total_hists[sample_name] = [[]]
                passed_hists[sample_name] = [[]]

                # for sample_dsid in ds_ids:
                infile = histogram_dir + config.samples[sample_name]["filepath"]
                if noweight:
                    total_hist = f"ftag/{config.map_folder}/{flav}_total_{jetcol}_no_event_weight"
                    pass_hist = f"ftag/{config.map_folder}/{tagger}_{wp}_{jetcol}_{flav}_pass_no_event_weight"
                else:
                    total_hist = f"ftag/{config.map_folder}/{flav}_total_{jetcol}"
                    pass_hist = (
                        f"ftag/{config.map_folder}/{tagger}_{wp}_{jetcol}_{flav}_pass"
                    )

                total_hists[sample_name][0].append([infile, total_hist])
                passed_hists[sample_name][0].append([infile, pass_hist])

                map_config["total_hists"] = total_hists
                map_config["passed_hists"] = passed_hists
                wr += 1
            map_configs[map_name] = map_config

        # continuous binning
        if wp == "Continuous":
            print(tagger)
            print(config.get_tagweight_bins(tagger))
            map_config["tagweight_bins"] = config.get_tagweight_bins(tagger)

    config_file = open(maps_filename, "w")
    config_file.write(json.dumps(map_configs, indent=3, sort_keys=True))

    print("Wrote ", wr, " map configurations ")
    return maps_filename


def main():
    args = parse_args()

    config = Configuration(args.config)

    create_maps_config(config=config, maps_tag=args.tag, noweight=args.no_weight)


if __name__ == "__main__":
    main()
