ITEMS=(
    "DL1dv01_Continuous_B_eta={0.0-2.5}_\[20-30\].png"
    "DL1dv01_Continuous_C_eta={0.0-2.5}_\[20-40\].png"
    "DL1dv01_Continuous_L_eta={0.0-2.5}_\[20-50\].png"
    "DL1dv01_FixedCutBEff_77_B_eta={0.0-2.5}.png"
    "DL1dv01_FixedCutBEff_77_C_eta={0.0-2.5}.png"
    "DL1dv01_FixedCutBEff_77_L_eta={0.0-2.5}.png"
    "GN120220509_Continuous_B_eta={0.0-2.5}_\[20-30\].png"
    "GN120220509_Continuous_C_eta={0.0-2.5}_\[20-40\].png"
    "GN120220509_Continuous_L_eta={0.0-2.5}_\[20-50\].png"
    "GN120220509_FixedCutBEff_77_B_eta={0.0-2.5}.png"
    "GN120220509_FixedCutBEff_77_C_eta={0.0-2.5}.png"
    "GN120220509_FixedCutBEff_77_L_eta={0.0-2.5}.png"
)


for MYPATH in ${ITEMS[@]}; do
    echo $MYPATH
    find vr_eff_maps/calibration_binned/*/ -name ${MYPATH} -exec cp {} vr_eff_maps \;
done
#
##MYPATH="DL1dv01_Continuous_B_eta={0.0-2.5}_[20.*30.].png"

# -exec cp {} outputs/ \;
