# Grid Config scripts
The following scripts produce the derivation histogram files and contain the config scripts necessary for PFlow jets. These are located in the directory 'grid_scripts'.

sample_list_* contain the MC files that are of interest.

config_* are the config files corresponding to PFlow jets

In `configuration.yaml` the following can be modified for submission:
- `username` should be changed to your username for submission to the Grid;
- `tag` should be changed for every submission as that indicates the version;
- `samples` contains the samples to run over
    - `sample list` should be a `txt` file with the MC samples;
    - `config_file` should be a config file corresponding to these samples;
    - `prw_file` should be a path to the used PRW file (the script will change the config file in accordance with it) -- **OPTIONAL**;
    - `period` should be a period of the used MC samples.

To send the job to the grid, simply do the following:

```
cd grid_scripts/
source setup.sh
python submit.py -c <path to configuration.yaml>
```
To get help use 
```
python submit.py -h
```

The jobs should be sent to the Grid for processing.

# Description of Efficiency Map Production Script
The following script produces root files containing histograms based of binning type (finely or calibrated binning), by dimension in terms of efficiency, eta and pt.

The script make_effmaps.py is an automatized script that will create a config file from derivations provided, extract information to produce histograms and store them in root files, as well as pdfs of efficiency maps stacked in terms of generators.

Each component of the efficiency map process can also be individually executed.

All explicit terms in terms of binning, flavours, taggers, jet collections and working points are listed and can be changed in the `configs/config.yaml` file.

`configs/config_reduced.py` contains configuration with the reduced number of generators and taggers.
These config.yaml files contain the samples metadata used for plotting. A sample for Powheg+Pythia8 must be in the samples with the name "PhPy8EG" as it is used as the nominal sample to compute ratios.

<u>To have these scripts work you need to install the following packages</u>:
- `ROOT`
- `puma-hep`
- `numpy`
- `matplotlib`
- `tqdm`
- `pyyaml`
- `atlas-ftag-tools`

You can [set up a recent ROOT version from `cvmfs`](https://lcgdocs.web.cern.ch/lcgdocs/lcgreleases/introduction/) with a virtual environment such that you can install the additional packages on top.

A `setup.sh` script provides the required functionality.

```
source setup.sh
```


## Fully Automatized Script
To execute make_effmaps.py, in your terminal type:

```
python make_effmaps.py --config <path to config file> --bin_type <c or f> --tag <tag> --run_all
```

- `--config` defines a path to your config file
- `--bin_type` defines the type of binning you want to use. ‘f’ is for finely binned while ‘c’ is for calibration binned. Calibration binned is the default.
- `--tag` defines what tag you choose for the map config file version (output name will look like `maps_config_ATofficial_tag.json`)

To get a full help please launch
```
python make_effmaps.py -h
```

## Config File Script
The script create_inputs_AT.py creates the config file for the efficiency map production from derivations. Datasets to be used need to be manually added in the datasets dictionary, where the keys correspond with the generator. The histogram_dir points to the directory containing the datasets.

The jet collection (jetCols), flavour, taggers, workingpoints and map folder used are all defined in the config file. That is the only location where these are explicitly defined. Select or write in any tagging information you feel in there.

To run this script type in your terminal:

```
python make_effmaps.py --config <path to config file> --bin_type <c or f> --tag <tag> --prepare
```

or 

```
python create_inputs_AT.py --config <path to config file> --bin_type <c or f> --tag <tag>
```

## Efficiency Map Root Files
Second file that is executed is create_eff.py, this will produce the root files that the next two scripts will run over. The outputted root files are:

- 2D eta vs pt maps
- 1D efficiency vs pt maps
- 1D efficiency vs eta maps
- 3D eta vs pt continuous maps
- 1D efficiency vs tag weight (for fixed eta and pt)
- 1D total counts vs pt and eta
- 1D passed counts vs pt and eta

To run this script type in your terminal:

```
python make_effmaps.py --config <path to config file> --bin_type <c or f> --tag <tag> --create
```

or 

```
python create_eff.py --config <path to config file> --maps <maps_name> --bin_type <c or f> --tag <tag>
```
Where `<maps_name>` is the file with maps config from the previous step.

## Stacked Efficiency Maps for Generators
The next script produces 1D efficiency maps stacked in terms of generators as pdfs.

To run this script type in your terminal:

```
python make_effmaps.py --config <path to config file> --bin_type <c or f> --tag <tag> --plot_maps
```
or
```
python plot_1d_maps.py --config <path to config file> --bin_type <c or f> --tag <tag>
```

The final script 2d_maps.py produces pdfs of the 2D eta vs pt maps that were produced by create_eff.py.

To run this script type in your terminal:

```
python make_effmaps.py --config <path to config file> --bin_type <c or f> --tag <tag> --plot_2d_maps
```
or
```
python plot_2d_maps.py --config <path to config file> --bin_type <c or f> --tag <tag>
```

## Validation script
Use the script validation.py in order to determine if histograms in root files contain any empty bins. The empty bin numbers of each histogram are outputted in the terminal.

To run this script type in your terminal:

```
python validation.py <root file path>
```
## Create CDI
Use the script create_CDI.py to build a CDI. If the calibration and finely binned files are not merged, the script will merge them with hadd. This merger can be done independently.

To run this script type in your terminal:

```
python create_CDI.py <input root file> <config file> <output file name>
```
## CDI Validation script
Use the validation_script.py to validate the smoothed CDI.

To run the script, include on line 52 the smooth CDI file path of the script. Then type in your terminal:

```
python validation_script.py
```
