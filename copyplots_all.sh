GLOBALPATH="vr_eff_maps/calibration_binned/"
ITEMS=(
    "eff_vs_eta"
    "eff_vs_pt"
    "eff_vs_wp"
)
TAGGERS=(
    "GN1"
    "DL1d"
)


for MYPATH in ${ITEMS[@]}; do
    echo $MYPATH
    for TAGGER in ${TAGGERS[@]}; do
        echo $TAGGER
        mkdir -p vr_eff_maps/calibration_binned/${MYPATH}/plots/${TAGGER}
        find vr_eff_maps/calibration_binned/${MYPATH}/${TAGGER}/* -name "*.png" -exec cp {} vr_eff_maps/calibration_binned/${MYPATH}/plots/${TAGGER} \;
    done
done